"""
main/views.py

Contoller Responsible For handling the Main Page and Dashboard

"""

from flask import Flask, render_template, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_user, logout_user, login_required, current_user
import os

from hackathon import db
from hackathon.main import main_bp
from hackathon.models import Participant, Submission, Team


@main_bp.route('/')
def homepage():
    """
    <url>/
    
    View that Renders the Homepage
    
    """
    return render_template('index.html')


@main_bp.route('/dashboard')
@login_required
def dashboard():
    """
    <url>/dashboard

    View that Returns the dashboard with the Projects created, Projects that are joined

    """
    return render_template('dashboard.html', submissions=Submission.query.filter_by(author=current_user.id), projects=Submission.query.all(), teams=Team.query.filter_by(participantId=current_user.id).all())
