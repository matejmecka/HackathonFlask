"""
forms.py

Forms used in the Hackathon Platform

"""
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField, IntegerField
from wtforms.validators import DataRequired, Email, URL


class registerForm(FlaskForm):
    """
    Registration Form

    Used to create new users

    firstName(String, First Name of User)
    lastName(String, Last Name of User)
    email(String, Email of User)
    password(String, Email of User)
    location(String, Location of User)

    """
    firstName = StringField('First Name', validators=[
                            DataRequired()], render_kw={"placeholder": "John"})
    lastName = StringField('Last Name', validators=[
                           DataRequired()], render_kw={"placeholder": "Snow"})
    email = StringField('Email', validators=[DataRequired(), Email()], render_kw={
                        "placeholder": "johnsnow@thenorth.tn"})
    password = PasswordField('Password', validators=[DataRequired()], render_kw={
                             "placeholder": "correcthorsebatterystaple"})
    location = StringField('Location', render_kw={"placeholder": "The North"})
    submit = SubmitField('Submit')


class loginForm(FlaskForm):
    """
    Login Form

    Used to Log In Students

    email(String, Email associated with account)
    password(String, Used for Verification)

    """
    email = StringField('Email', validators=[DataRequired(), Email()], render_kw={
                        "placeholder": "johnsnow@thenorth.tn"})
    password = PasswordField('Password', validators=[DataRequired()], render_kw={
                             "placeholder": "correcthorsebatterystaple"})
    submit = SubmitField('Submit')


class projectForm(FlaskForm):
    """
    Create Project Form

    Used to create projects

    name(String, Name of project)
    shortDesc(String, Short description of project with 120 charatchers)
    url(String, URL Where project can be found)
    Stack(String, What the project is built with)
    Platforms(String, Supported Platforms)

    """
    name = StringField('Name of Project', validators=[
                       DataRequired()], render_kw={"placeholder": "The North"})
    shortDesc = StringField('Short Description of your Project', validators=[DataRequired(
    )], render_kw={"placeholder": "The quick  brown fox...", "max_length": 120})
    description = StringField('Description', validators=[DataRequired()], render_kw={
                              "placeholder": "Lorem ispum sit dolor amet..."})
    url = StringField('Link to Project', validators=[
                      DataRequired(), URL()], render_kw={"placeholder": "https://github.com/"})
    stack = StringField('Built with', validators=[DataRequired()], render_kw={
                        "placeholder": "NodeJS, Express..."})
    platforms = StringField('Platforms', validators=[DataRequired()], render_kw={
                            "placeholder": "Web, Desktop, Mobile App(Android, iOS) etc..."})
    submit = SubmitField('Submit')


class joinTeamForm(FlaskForm):
    """
    Join Team Form

    used to join a team

    teamCode(Integer, The code that will be used to querry the database)

    """
    teamCode = IntegerField('Team Code', validators=[DataRequired()], render_kw={
                            "placeholder": "43219043"})
    submit = SubmitField('Submit')
