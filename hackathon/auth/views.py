"""
auth/views.py

Controller Responsible for Handling all Authentication
related functions.

"""
from flask import render_template, redirect, request, url_for, flash
from flask_login import login_user, logout_user, login_required, current_user
from werkzeug.security import generate_password_hash
from hackathon.auth import auth_bp
from hackathon import db
from hackathon.models import Participant
from hackathon.forms import registerForm, loginForm


@auth_bp.route('/register', methods=['GET', 'POST'])
def signup():
    """
    <url>/register

    View that registers new users into the platform

    """
    form = registerForm()
    if form.validate_on_submit():
        user = Participant(firstName=form.firstName.data, lastName=form.lastName.data, email=form.email.data,
                           password_hash=generate_password_hash(form.password.data), location=form.location.data, role_id=2)
        existingUser = Participant.query.filter_by(
            email=form.email.data).first()
        if existingUser is None:
            db.session.add(user)
            db.session.commit()
            login_user(user)
            flash('Registration Succesfull!', 'alert-success')
            return redirect(url_for('main.dashboard'))
        else:
            print('User Exists')
            flash('User Already Exists!', 'alert-danger')
    return render_template('register.html', form=form)


@auth_bp.route('/login', methods=['GET', 'POST'])
def login():
    """
    <url>/login

    View that logs users into the Platform

    """
    form = loginForm()
    if form.validate_on_submit():
        user = Participant.query.filter_by(email=form.email.data).first()
        print(user)
        if user is not None and user.verify_password(form.password.data):
            login_user(user)
            #next = hackathon.request.args.get('next')
            return redirect(url_for('main.dashboard'))
            # if not is safe_url(next):
            #    return flask.abort(400)

        else:
            flash('Incorrect Email or Password!', 'alert-danger')
    return render_template('login.html', form=form)


@auth_bp.route("/logout")
@login_required
def logout():
    """
    <url>/logout
    
    A View which logs users out and redirects to the homepage

    """
    logout_user()
    flash('Logout Succesfull!', 'alert-success')
    return redirect(url_for('main.homepage'))
