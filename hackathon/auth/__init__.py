from flask import Blueprint
auth_bp = Blueprint('auth', __name__)

from hackathon.auth import views
from hackathon.models import Participant
