from flask import Blueprint
projects_bp = Blueprint('projects', __name__)

from hackathon.projects import views
from hackathon.models import Participant, Submission, Role
