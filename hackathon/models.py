"""
models.py

Responsible for all Models that is used for the Hackathon Platform

"""

from sqlalchemy import Column, Integer, String, Text, DateTime
import datetime
import bleach
from werkzeug.security import generate_password_hash, check_password_hash
from markdown import markdown
from flask_login import UserMixin
from . import db, login_manager


@login_manager.user_loader
def get_user(ident):
    """
    Used by Flask Login to login users
    
    ident(Users Identity)
    """
    return Participant.query.get(int(ident))


class Role(db.Model):
    """
    Database Model for Representing Participant's role

    id(Integer, Unique and Primary Identifier for Roles)
    name(String, Unique, Names to Represent Roles)
    
    """
    __tablename__ = "roles"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)

    def __repr__(self):
        return'<Role {}>'.format(self.name)


class Participant(UserMixin, db.Model):
    """
    Database Model to represent Participants
    
    id(Integer, Unique and Primary Identifier for Submissions)
    firstName(String, The First Name of the User)
    lastName(String, The Last Name of the User)
    email(String, User's Email, Used for Authentication and contact)
    password_hash(String, Used for Authentication)
    location(String, Users Location)
    role_id(ForeignKey, Users role)
    created(DateTime, Time the Account was created) 

    verifyPassword(Returns a Boolean if the hash equals to the submitted one)

    """
    __tablename__ = "participant"
    id = db.Column(db.Integer, primary_key=True)
    firstName = db.Column(db.String(64))
    lastName = db.Column(db.String(64))
    email = db.Column(db.String(64), unique=True)
    password_hash = db.Column(db.String(256), unique=True)
    location = db.Column(db.String(64), unique=True)
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    created = db.Column(db.DateTime(timezone=True),
                        default=datetime.datetime.utcnow)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return'<Participant {}>'.format(self.firstName + ' ' + self.lastName)


class Submission(db.Model):
    """
    A Database Model for Representing Each Submission
    
    id(Integer, Unique and Primary Identifier for Roles)
    name(String, The Name of the Project)
    shortDesc(String, A Short Description of the project in 120 characthers)
    description(String, Description where you can express yourself more, The limit is 800 charatchers)
    url(String, URL of where the project can be found)
    stack(String, The Technologies used for the project)
    platforms(String, Which Platforms does the Project Support)
    code(Integer, a Unique random 8 number code used to join other projects)
    author(ForeignKey, Used to Link the publisher to the Project)
    votingEnabled(Integer, Boolean, Used to Specify which Submissions can have their votes enabled)
    created(Datetime, When the project was created)

    """
    __tablename__ = "Submission"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    shortDesc = db.Column(db.String(120))
    description = db.Column(db.String(800))
    url = db.Column(db.String(128))
    stack = db.Column(db.String(128))
    platforms = db.Column(db.String(128))
    code = db.Column(db.Integer, unique=True)
    author = db.Column(db.Integer, db.ForeignKey('participant.id'))
    votingEnabled = db.Column(db.Integer, default=0)
    created = db.Column(db.DateTime(timezone=True),
                        default=datetime.datetime.utcnow)

    def __repr__(self):
        return'<Submission {}>'.format(self.name)


class Team(db.Model):
    """
    A Database Model for Representing Teamates in Projects
    
    id(Integer, Unique and Primary Identifier for Teamates)
    projectId(ForeignKey, The Project ID Which the Teamate is Part of)
    participantId(ForeignKey, The Participant ID which represents which part of the Project Is)

    """
    __tablename__ = "Team"
    id = db.Column(db.Integer, primary_key=True)
    projectId = db.Column(db.Integer, db.ForeignKey('Submission.id'))
    participantId = db.Column(db.Integer, db.ForeignKey('participant.id'))

    def __repr__(self):
        return '<Team {}>'.format(self.id)


class Votes(db.Model):
    """
    A Database Model for Representing Votes

    id(Integer, Unique and Primary Identifier for Votes)
    projectId = (ForeignKey, The Project ID Which the User votes for)
    participantId = (ForeignKey, The Participant ID for which the Project he/she voted for)
    voted = (DateTime, Time User Voted for the Project)
    """
    __tablename__ = 'votes'
    id = db.Column(db.Integer, primary_key=True)
    projectId = db.Column(db.Integer, db.ForeignKey('participant.id'))
    participantId = db.Column(db.Integer, db.ForeignKey('participant.id'))
    voted = db.Column(db.DateTime(timezone=True),
                      default=datetime.datetime.utcnow)

    def __repr__(self):
        return'<Vote {}>'.format(self.name)
