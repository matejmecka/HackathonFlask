# Hackathon

## Environment setup

  * Clone this repo
  * Install Python 3 (3.6 recommended)
  * Create a virtual environment: `python3 -m venv env`
  * Activate the virtualenv: `source env/bin/activate`
  * Install dependencies: `pip install -r requirements.txt`
  * Set the flask app environment variables: `set FLASK_APP=manage.py`
    * This can be added to the end of 'env/bin/activate' to happen automatically whenever you activate the venv
  * Apply database migrations: `flask db upgrade`
  
